# Lista de Exercícios

- Por favor, leia os exercícios e faca apenas os que achar que NÃO sabe fazer! Se a resposta estiver correta, parabéns você aprendeu! Se der alguma coisa muito estranha... Ou voce é um gênio e descobriu uma coisa nova na Eletrônica... Ou precisa de ajuda -> Pergunte aos colegas... ou venha falar comigo!


### 1) Considere o circuito da figura (https://tinyurl.com/2qyukj5d) 
- Qual é a resistencia equivalente do circuito?
- Qual é a corrente total do circuito?
- Qual é a corrente no segundo resistor (esquerda para direita)?

### 2) Considere o circuito da figura (https://tinyurl.com/2lduqzmg)
- Porque que com um "grafite" de 10 ohms produzimos o maior calor possível?

### 3) Portas lógicas no FALSTAD (nMOS):
- Desenhe uma porta NOT com Relés.
- Desenhe uma porta AND com Relés.
- Desenhe uma porta OR com Relés.
- Desenhe uma porta NOT com Transistores nMOS.
- Desenhe uma porta AND com Transistores nMOS.
- Desenhe uma porta OR com Transistores nMOS.

### 4) Portas lógicas no TINKERCAD  (nMOS):
- Crie uma conta no TINKERCAD - https://www.tinkercad.com/things/igRyWCnxLIV?sharecode=mRvoU8mG4KO6SirQdPU_t78wHfWxr6fDnbhioYeESxg
- Desenhe uma porta NOT com Transistores nMOS.
- Desenhe uma porta AND com Transistores nMOS.
- Desenhe uma porta OR com Transistores nMOS.

### 5) Portas lógicas no FALSTAD (cMOS):
- Desenhe uma porta NOT com Transistores cMOS.
- Desenhe uma porta AND com Transistores cMOS.
- Desenhe uma porta OR com Transistores cMOS.

### 6) Portas lógicas no TINKERCAD  (cMOS):
- Desenhe uma porta NOT com Transistores cMOS.
- Desenhe uma porta AND com Transistores cMOS.
- Desenhe uma porta OR com Transistores cMOS.

### 7) Portas lógicas no FALSTAD:
- Desenhe uma porta XOR com Transistores cMOS com soma de produtos.
- Desenhe uma porta XOR com Transistores cMOS otimizada com portas (OR, NAND e AND).

### 8) LEDs
- Considere o seguinte LED : (https://www.baudaeletronica.com.br/led-difuso-10mm-vermelho.html)
  - Cor	        Vermelho
  - Diâmetro	10mm
  - Tensão 	    2V
  - Corrente 	20mA

- Calcule quais resstores seriam necessários para ligar este LED com as seguintes tensões:
  - 5V, utilizar	resistor de 150R
  - 6V, utilizar 	resistor de 200R
  - 9V, utilizar	resistor de 360R
  - 12V, utilizar   resistor de 560R
  - 15V, utilizar 	resistor de 680R
  - 24V, utilizar	resistor de 1K1
 	 

