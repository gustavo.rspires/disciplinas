RIPPLE:

Vs = Tensao Maxima (Vpico)
f  = Frequencia (f=60Hz)

Fórmula Simplificada de Calcular o Ripple de até 20%  --> Mais que isso a equacao começa a errar bastante

Ripple para meia onda: RIPPLE =  Vs / f * C * R

Ripple para Onda completa: RIPPLE =  Vs / 2*f * C * R

Formula mais Precisa: RIPPLE = Vs*(1-e^(-1/FRC))

---x--------x-------
Exemplo 1:
Para Vs = 10v e f = 60Hz
          C = 84uF e R = 1K

Ripple = 10 / 120*84x10-6*1000 = 0.99V => 10%

Pela formula Precisa: Ripple = 10*(1-e^-1/120*1000*82x10-6) = 1V

---x--------x-------
Exemplo 2:
Usando os conhecimentos anteriores e a Equação de Decaimento da tensão no Capacitor:
VC = Vs / e^(t/RC)

Decaimento de 10% (Ripple = 10%) -> se a Vs era de 100V, teria que decair a até 90V 
90 = 100 / e^(t/RC)

t/RC = ln (100/90)

t/RC = 0.1

t = 0.1RC

t = 7ms = 0.007s   ->  esse é o tempo que a tensão decai até encontrar a subidinha da senoide em 90% da V de pico:  mais ou menos 7ms

e Meio Periodo, que é o tempo entre dois picos das duas montanhas (onda Completa) é:
T = 1/f = 1/60 = 0.0166s
T/2 = 0.0083s 

Sendo assim:
t = 0.1RC  -->  RC = 0.007 / 0.1 = 0,07

Então, se fixarmos R=1k, C tem que ser:
1000*C = 0.07
C = 0.07 / 1000 = 70uF


--------x------------x----------
Exemplo 2: Dados do simulador da nossa FONTE com transformador, ponte de diodos e capacitor:
Vs=16.477
Vmin = 14.6

f=60Hz --> t entre Vs e Vmin = 7ms

R = 180
C = 330uF

Vc = Vs/e^(t/RC)
Vc = 16.477 / e^(0.007/180*330x10-6) = 14,64


--------x------------x-------
RC Filter Cut-off Frequency => Fc = 1/2PI*RC  em Hz
FC e a frequencia que a reducao do filtro ser 70.7% da Original Voltage ==> perda de 3dB
ex. Para C = 1uF e R = 1K
FC = 1/ 6,28 * 1000 * 1x10^-6 = 159 Hz
