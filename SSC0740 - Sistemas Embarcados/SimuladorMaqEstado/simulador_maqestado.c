// Maquina de Estador implementada com Switch !!!
// gcc simple_simulator.c -O3 -march=native -o simulador -Wall


// Estados da Maquina
#define STATE_DESATIVADO 0
#define STATE_ATIVADO 1
#define STATE_DISPAROU 2

//----------------

//#include <curses.h>     //  Novo Terminal cheio de funcoes!!!
#include <stdlib.h>     // Rand
#include <stdio.h>      // Printf

// kbhit() TODO: deletar
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>


int kbhit(void)
{
  struct termios oldt, newt;
  int ch;
  int oldf;
 
  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);
 
  ch = getchar();
 
  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  fcntl(STDIN_FILENO, F_SETFL, oldf);
 
  if(ch != EOF)
  {
    ungetc(ch, stdin);
    return 1;
  }
 
  return 0;
}


int main()
{
	int key=0;    // Le Teclado
	int LED = 0;  
	int Bz = 0;
	int botao = 0;
	int porta = 0;
	unsigned char state = 0, state_anterior = 0; // reset
	
	printf("Alarme do Carro - 'r' Reset, 'q' Quit, 'b' Botao, 'p' Porta\n");

inicio:
	printf("\nRodando...\n");

	state = STATE_DESATIVADO;
	printf("\rESTADO = %d, LED = %d, Bz = %d", state, LED, Bz );


	// Loop principal do processador: Nunca para!!
loop:
	// Zera todos os Sinais de Saida: -Para liga-los individualmente depois dentro dos estados
	LED = 0;  
	Bz = 0;


	// Lógica de Transicao
	switch(state)
	{
		case STATE_DESATIVADO:
			
			if(botao) state=STATE_ATIVADO;
			break;

		case STATE_ATIVADO:

			if(botao) state=STATE_DESATIVADO;
			if(porta) state=STATE_DISPAROU;
			break;

		case STATE_DISPAROU:

			if(botao) state=STATE_DESATIVADO;
			break;
	}


	// Lógica de Acao
	switch(state)
	{
		case STATE_DESATIVADO:
			
			LED = 0;  
			Bz = 0;
			break;

		case STATE_ATIVADO:

			LED = 1;  
			Bz = 0;
			break;

		case STATE_DISPAROU:

			LED = 1;  
			Bz = 1;
			break;
	}


// Bloco de Imput
	if(botao) botao=0;
	if(porta) porta=0;

	if(kbhit()) 
		{
		key=getchar();
		if     (key=='b') botao=1;
		else if(key=='p') porta=1;
		else if(key=='q') goto fim;
		else if(key=='r') goto inicio;

		}

// Bloco de output
	if(state!=state_anterior)  // Por Civilidade, So' imprime se a situacao mudar!!
		{
			state_anterior=state;

			printf("\rESTADO = %d, LED = %d, Bz = %d", state, LED, Bz );
		}
	goto loop;

fim:
	printf("\n\nMake Love, don't Make War !!\n\n");

	return 0;
}


